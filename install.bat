@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "scripts\install_chocolatey.ps1" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

echo "Installing Version Control tools"
echo "===================================================================="
echo "Installing Git"
choco install git -y
echo "Installing TortoiseGit"
choco install tortoisegit
echo "Installing TortoiseSVN"
choco install tortoisesvn -y

echo "Installing Virtualization environment"
echo "===================================================================="
echo "Installing VirtualBox"
choco install virtualbox -y
REM echo "Installing Vagrant"
REM choco install vagrant -y
REM echo "Installing Docker for Windows"
REM choco install docker-for-windows -y
REM echo "Installing Docker CLI"
REM choco install docker -y
REM echo "Installing Docker-Compose"
REM choco install docker-compose -y
REM echo "Installing Kubernetes CLI (kubectl)"
REM choco install kubernetes-cli -y
REM echo "Installing Minikube"
REM choco install minikube -y


echo "Installing Java tooling"
echo "===================================================================="
echo "Installing JDK 8"
choco install jdk13 -y
echo "Installing JRE 8"
choco install jre13 -y
echo "Installing Maven"
choco install maven -y
echo "Installing Gradle"
choco install gradle -y
REM echo "Installing Eclipse IDE"
REM choco install eclipse -y

echo "Installing JS tooling"
echo "===================================================================="
echo "Installing NodeJS"
choco install nodejs -y

REM echo "Installing .NET tooling"
REM echo "===================================================================="
REM echo "Installing .NET 4.5"
REM choco install dotnet4.5 -y
REM echo "Installting Dot NET Core SDK"
REM choco install dotnetcore-sdk -y
REM echo "Installing SQL Server Management Studio"
REM choco install sql-server-management-studio -y
REM echo "Installing Visual Studio 2017 Enterprise"
REM choco install visualstudio2017enterprise -y
REM echo "Installing Visual Studio 2017 Professional"
REM choco install visualstudio2019professional --pre -y
REM echo "Installing Database.NET"
REM choco install databasenet -y


echo "Installing complementary development tools"
echo "===================================================================="
echo "Installing Cmder (console/terminal emulator)"
choco install cmder -y
echo "Installing 7ZIP"
choco install 7zip.install -y
echo "Installing Notepad++"
choco install notepadplusplus -y
echo "Installing Visual Studio Code"
choco install visualstudiocode -y
echo "Installing WinMerge"
choco install winmerge -y
echo "Installing Putty"
choco install putty.install -y
echo "Installing ProceExp"
choco install procexp -y
echo "Installing WGET"
choco install wget -y
echo "Installing cURL"
choco install curl -y
echo "Installing Rufus"
choco install rufus -y
echo "Installing Greenshot"
choco install greenshot -y
echo "Installing ScreenToGif"
choco install screentogif -y
echo "Installing Adobe Reader"
choco install adobereader -y
echo "Installing FileZilla"
choco install filezilla -y
echo "Installing Wireshark"
choco install wireshark -y
echo "Installing Chrome browser"
choco install googlechrome -y

REM echo "Installing cloud tools"
REM echo "===================================================================="
REM echo "Installing AWS CLI"
REM choco install awscli -y
REM echo "Installing Gcloud SDK"
REM choco install gcloudsdk -y
REM echo "Installing Azure CLI"
REM choco install azure-cli -y
